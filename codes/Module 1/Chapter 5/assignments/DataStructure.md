## Breadth First Search (BFS)

BFS is similar to level order traversal in a tree, where we visit vertices in a level by level order. We specify a starting vertex _S_ and start visiting vertices of the graph _G_ from the vertex S. As a general graph can have cycles, we may visit the same vertex more than once. To solve this, we also maintain the state of each vertex. A vertex can be in one of three states, VISITED, NOT_VISITED, IN_PROCESS. The basic idea of breadth first search is to find the least number of edges between S and any other vertex in G. Starting from _S_, we visit vertices of distance k before visiting any vertex of distance _k+1_. For that purpose, define ds(v) to be the least number of edges between _S_ and _v_ in _G_. So, for vertices v that are not reachable from S we can have _ds(v) = ∞._ We can use a queue to store vertices in progress.

We also a maintain _from[u]_ for each of the vertex u, which denotes the vertex that discovered u. This information can be used to define the predecessor subgraph of G, which has all edges _(from[u],u)._ These edges are called as tree edges. For a tree edge _(u, v), ds(v) = ds(u) + 1._ All other edges are called non-tree edges and are further classified as back edges and cross edges. A non-tree edge _(u, v)_ is called as a back edge if _ds(v) < ds(u),_ meaning u can be reached from _S_ via _v_, but there is yet another shortest path through path u can be reached from _S_. An edge _(u, v)_ is called a cross edge if _d(v) ≤ d(u) + 1._

_**Procedure BFS(G)** <br>
for each v in V(G) do <br>
from[v] = NIL; state[v] = NOT_VISITED; d(v) = ∞ <br>
end-for_

_ds[S] = 0; state[S] = IN_PROCESS; from[s] = NIL; <br>
Q = EMPTY; Q.Enqueue(S);_

_While Q is not empty do  <br>
v = Q.Dequeue(); <br>
for each neighbour w of v do <br>
if state[w] == NOT_VISITED then <br>
state[w] = IN_PROCESS; from[w] = v; ds[w] = ds[v] + 1; Q.Enqueue(w); <br>
end-if <br>
state[v] = VISITED <br>
end-for <br>
end-while <br>
**End-BFS**_

<ins>*Runtime:*</ins> Each vertex enters the queue at most once and each edge is considered only once. Therefore, runtime of BFS is _O(m+n)_, where n is the number of vertices and m is the number of edges in G.

## Depth First Search (DFS)
The idea of DFS is to start from a specified start vertex S and explore from S as deep as possible. We go from S, to one of its neighbors x, to a neighbor of x, and so on. We stop when there are no new neighbors to explore from a given vertex. If all vertices are not visited, pick another start vertex from such vertices. We have to keep track of the state of a vertex and similar to BFS, a vertex can be in three states: VISITED, NOT_VISITED, IN_PROCESS. We also a maintain from[u], d[u], f[u] for each of the vertex u, which denotes the vertex that discovered u, the discovery time of u and the finish time u.

_**Procedure Explore(v)** <br>
d[v] = cur_time; <br>
cur_time = cur_time + 1; <br>
for each neighbor w of v <br>
if state(w) == NOT_VISITED then <br>
state(w) = IN_PROCESS; <br>
Explore(w); <br>
end-if <br>
end-for <br>
state(v) = VISITED; <br>
f[v] = cur_time; <br>
cur_time = cur_time + 1; <br>
**End-Explore**_

_**Procedure DFS(G)** <br>
cur_time = 0 <br>
for v = 1 to n do <br>
if state(v) == NOT_VISITED then <br>
state(v) = IN_PROCESS; <br>
Explore(v); <br>
end-if <br>
end-for <br>
**End-DFS**_

<ins>_Runtime:_</ins>Explore() is called for each vertex exactly once and each edge is considered only once. Therefore, runtime of DFS is O(m+n), where n is the number of vertices and m is the number of edges in G.

Classification of edges, based on Depth First traversal is as follows

- Tree Edges : All edges (from[u],u) are called as tree edges and define a dfs tree, a subgraph of G.
  - An edge (u,v) is a tree edge if from[v] = u

- Back Edges : A non-tree edge (u, v) is called as a back edge if v is an ancestor of u in the dfs tree. u can be reached from v using tree edges, but there an edge from u to v also.
  
  - An edge (u,v) is a back edge if [d(u), f(u)] is a subinterval of [d(v), f(v)]

- Forward Edges : A non-tree edge (u, v) is called as a forward edge if v is a descendant of u in the dfs tree. v can be reached from u using tree edges, but there an edge from u to v also.
  - An edge (u,v) is a forward edge if [d(v), f(v)] is a subinterval of [d(u), f(u)].

- Cross Edges : Non-tree edges (u, v) where u and v do not share any ancestor/descendant relationship are called cross edges.
  - An edge (u,v) is a cross edge if the two intervals [d(u), f(u)] and [d(v), f(v)] do not overlap.