var n1 = document.getElementById("n1");
var n2 = document.getElementById("n2");
var ans = document.getElementById("ans");
function condition() {
    if (n1.value == " " && n2.value == " ") {
        alert("Please Enter Both Numbers");
    }
    else if (n1.value == " ") {
        alert("Please Enter Number1");
    }
    else if (n2.value == " ") {
        alert("Please Enter Number2");
    }
    else if (isNaN(parseFloat(n1.value)) || isNaN(parseFloat(n2.value))) {
        alert("Please Enter Numbers Only");
    }
    else {
        return 1;
    }
}
function add() {
    var isOK = condition();
    if (isOK) {
        var a = parseFloat(n1.value) + parseFloat(n2.value);
        ans.value = a.toString();
    }
}
function sub() {
    var isOK = condition();
    if (isOK) {
        var a = parseFloat(n1.value) - parseFloat(n2.value);
        ans.value = a.toString();
    }
}
function mul() {
    var isOK = condition();
    if (isOK) {
        var a = parseFloat(n1.value) * parseFloat(n2.value);
        ans.value = a.toString();
    }
}
function div() {
    var isOK = condition();
    if (parseFloat(n2.value) == 0) {
        alert("Division By 0 is not possible");
    }
    else {
        if (isOK) {
            var a = parseFloat(n1.value) / parseFloat(n2.value);
            ans.value = a.toString();
        }
    }
}
//# sourceMappingURL=app.js.map