function dtable() {
    var input = document.getElementById("input");
    var table = document.getElementById("table_1");
    if (input.value == "") { //Checking wheather user entered any value or not.
        alert('Please enter any value!');
    }
    else if (isNaN(parseFloat(input.value))) { //Checking wheather user entered number or not.
        alert("Please enter number only!");
    }
    else {
        var num = +input.value;
        var count = 1;
        var t = 1;
        while (table.rows.length > 1) { //This loop deletes previous data(rows) to gennerate new table.
            table.deleteRow(1); //Deleted row one by one.
        }
        for (count = 1; count <= num; count++) { //This loop inserts rows & columns and generates multiplication table upto the number entered by the user.
            var row = table.insertRow(); //Inserting row
            var cell = row.insertCell(); //Inserting cell
            var text = document.createElement("input");
            text.type = "text";
            text.id = "t" + count;
            text.value = num.toString();
            text.style.textAlign = "center";
            cell.appendChild(text); //Adding text into the cell
            var cell = row.insertCell(); //Inserting cell
            var text = document.createElement("input");
            text.type = "text";
            text.id = "t" + count;
            text.value = "*";
            text.style.textAlign = "center";
            cell.appendChild(text); //Adding text into the cell
            var cell = row.insertCell(); //Inserting cell
            var text = document.createElement("input");
            text.type = "text";
            text.id = "t" + count;
            text.value = count.toString();
            text.style.textAlign = "center";
            cell.appendChild(text); //Adding text into the cell
            var cell = row.insertCell(); //Inserting cell
            var text = document.createElement("input");
            text.type = "text";
            text.id = "t" + count;
            text.value = "=";
            text.style.textAlign = "center";
            cell.appendChild(text); //Adding text into the cell
            var cell = row.insertCell(); //Inserting cell
            var text = document.createElement("input");
            text.type = "text";
            text.id = "t" + count;
            text.value = (count * num).toString();
            text.style.textAlign = "center";
            cell.appendChild(text); //Adding text into the cell
        }
    }
}
//# sourceMappingURL=table.js.map