function isInside() {
    var t11 = document.getElementById("t11");
    var t12 = document.getElementById("t12");
    var t21 = document.getElementById("t21");
    var t22 = document.getElementById("t22");
    var t31 = document.getElementById("t31");
    var t32 = document.getElementById("t32");
    var t41 = document.getElementById("t41");
    var t42 = document.getElementById("t42");
    var ans = document.getElementById("ans");
    function conditions() {
        if (t11.value == "" || t12.value == "" || t21.value == "" || t22.value == "" || t31.value == "" || t32.value == "" || t41.value == "" || t42.value == "") {
            alert("Please enter coordinates for all vertices");
        }
        else if (isNaN(parseFloat(t11.value)) || isNaN(parseFloat(t12.value)) || isNaN(parseFloat(t21.value)) || isNaN(parseFloat(t22.value)) || isNaN(parseFloat(t31.value)) || isNaN(parseFloat(t32.value)) || isNaN(parseFloat(t41.value)) || isNaN(parseFloat(t42.value))) {
            alert("Please enter valid coordinates for all vertices.");
        }
        else {
            return true;
        }
    }
    var test = conditions(); //EXCEPTION HANDLING
    if (test) {
        var x1 = +t11.value;
        var y1 = +t12.value;
        var x2 = +t21.value;
        var y2 = +t22.value;
        var x3 = +t31.value;
        var y3 = +t32.value;
        var x = +t41.value;
        var y = +t42.value;
        var A = Math.abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2); // Calculates area of triangle ABC
        var A1 = Math.abs((x * (y2 - y3) + x2 * (y3 - y) + x3 * (y - y2)) / 2); // Calculates area of triangle PBC
        var A2 = Math.abs((x1 * (y - y3) + x * (y3 - y1) + x3 * (y1 - y)) / 2); // Calculates area of triangle PAC 
        var A3 = Math.abs((x1 * (y2 - y) + x2 * (y - y1) + x * (y1 - y2)) / 2); // Calculates area of triangle PAB
        var sum = A1 + A2 + A3; // Sum of A(PBC), A(PAC) and A3(PAB)
        if (A == sum) { // Checks if sum of A1, A2 and A3 is same as A
            ans.innerHTML = "The point lies inside the triangle";
        }
        else {
            ans.innerHTML = "The point lies outside the triangle";
        }
    }
}
//# sourceMappingURL=triangle.js.map